use paste::paste;
use stm32l4::stm32l4x2;

pub enum ADCResolution {
    _12Bits,
    _10Bits,
    _8Bits,
    _6Bits,
}

impl ADCResolution {
    pub fn bits(self) -> u8 {
        match self {
            Self::_12Bits => 0b00,
            Self::_10Bits => 0b01,
            Self::_8Bits => 0b10,
            Self::_6Bits => 0b11,
        }
    }

    pub fn full_scale(self) -> u16 {
        match self {
            Self::_12Bits => (1 << 12) - 1,
            Self::_10Bits => (1 << 10) - 1,
            Self::_8Bits => (1 << 8) - 1,
            Self::_6Bits => (1 << 6) - 1,
        }
    }

    pub fn from(bits: u8) -> Self {
        if bits == 0b00 {
            Self::_12Bits
        } else if bits == 0b01 {
            Self::_10Bits
        } else if bits == 0b10 {
            Self::_8Bits
        } else if bits == 0b11 {
            Self::_6Bits
        } else {
            panic!("No ADC resolution for {}", bits);
        }
    }
}

pub enum SamplingTime {
    _2_5Cycles,
    _6_5Cycles,
    _12_5Cycles,
    _24_5Cycles,
    _47_5Cycles,
    _92_5Cycles,
    _247_5Cycles,
    _640_5Cycles,
}

impl SamplingTime {
    pub fn bits(self) -> u8 {
        match self {
            Self::_2_5Cycles => 0b000,
            Self::_6_5Cycles => 0b001,
            Self::_12_5Cycles => 0b010,
            Self::_24_5Cycles => 0b011,
            Self::_47_5Cycles => 0b100,
            Self::_92_5Cycles => 0b101,
            Self::_247_5Cycles => 0b110,
            Self::_640_5Cycles => 0b111,
        }
    }
}

pub enum ADCPrescaler {
    Hclk,
    Hclk2,
    Hclk4,
}

impl ADCPrescaler {
    pub fn bits(self) -> u8 {
        match self {
            Self::Hclk => 0b01,
            Self::Hclk2 => 0b10,
            Self::Hclk4 => 0b11,
        }
    }
}

pub enum ClockMode {
    Asynchronous,
    Synchronous(ADCPrescaler),
}

pub enum ExternalTrigger {
    Disabled,
    OnRisingEdge,
    OnFallingEdge,
    OnBoth,
}

impl ExternalTrigger {
    pub fn bits(self) -> u8 {
        match self {
            Self::OnBoth => 0b11,
            Self::OnFallingEdge => 0b10,
            Self::OnRisingEdge => 0b01,
            Self::Disabled => 0b00,
        }
    }
}

pub struct ADC<'a> {
    pub rcc: &'a stm32l4x2::RCC,
    pub adc_common: &'a stm32l4x2::ADC_COMMON,
}

impl<'a> ADC<'a> {
    pub fn initialize(&self) -> &Self {
        // Enable ADC clock
        self.rcc.ahb2enr.modify(|_, w| w.adcen().set_bit());

        // Reset peripheral
        self.rcc
            .ahb2rstr
            .modify(|_, w| w.adcrst().set_bit().adcrst().clear_bit());

        self
    }

    pub fn derive_clock_from_ahb(&self, presc: ADCPrescaler) -> &Self {
        self.adc_common
            .ccr
            .modify(|_, w| unsafe { w.ckmode().bits(presc.bits()) });

        self
    }
}

macro_rules! adcs {
    ($($ADCx:expr),*) => {
        paste! {
            $(
                pub struct $ADCx<'a> {
                    pub [<$ADCx:lower>]: &'a stm32l4x2::$ADCx,
                }

                impl<'a> $ADCx<'a> {
                    /// Power up the ADC
                    ///
                    /// * `vreg_setup_delay` - Tadcvreg_stup is 20 usec
                    pub fn power_up<D>(&self, vreg_setup_delay: D) -> &Self
                    where
                        D: FnOnce() -> ()
                    {
                        // Exit deep-power-down and enable voltage regulator
                        self.[<$ADCx:lower>].cr.modify(|_, w| w.deeppwd().clear_bit().advregen().set_bit());

                        (vreg_setup_delay)();

                        self
                    }

                    /// Calibrate single ended and differential channels
                    ///
                    /// * `calibration_delay` - 4 ADC cycles are required after calibration finishes
                    pub fn calibrate<D>(&self, calibration_delay: D) -> &Self
                    where
                        D: FnOnce() -> ()
                    {
                        self.[<$ADCx:lower>].cr
                            .modify(|_, w| w.adcaldif().clear_bit().adcal().set_bit());

                        while self.[<$ADCx:lower>].cr.read().adcal().bit_is_set() {}

                        self.[<$ADCx:lower>].cr
                            .modify(|_, w| w.adcaldif().set_bit().adcal().clear_bit());

                        while self.[<$ADCx:lower>].cr.read().adcal().bit_is_set() {}

                        (calibration_delay)();

                        self
                    }

                    ///Enable the ADC
                    pub fn enable(&self) -> &Self {
                        self.[<$ADCx:lower>].isr.modify(|_, w| w.adrdy().set_bit());
                        self.[<$ADCx:lower>].cr.modify(|_, w| w.aden().set_bit());
                        while self.[<$ADCx:lower>].isr.read().adrdy().bit_is_clear() {}
                        self.[<$ADCx:lower>].isr.modify(|_, w| w.adrdy().set_bit());
                        self
                    }
                }
            )*
        }
    };
}

adcs!(ADC1, ADC2);
