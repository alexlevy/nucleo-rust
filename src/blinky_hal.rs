#![no_std]
#![no_main]

use crate::hal::delay::Delay;
use crate::hal::prelude::*;
use cortex_m::asm;
use cortex_m_rt::{entry, exception, ExceptionFrame};
use rtt_target::{rprintln, rtt_init_print};
use stm32l4xx_hal as hal;

fn with_hal() -> ! {
    let cp = cortex_m::Peripherals::take().unwrap();
    let dp = hal::stm32::Peripherals::take().unwrap();

    let mut flash = dp.FLASH.constrain();
    let mut rcc = dp.RCC.constrain();
    let mut pwr = dp.PWR.constrain(&mut rcc.apb1r1);

    // let clocks = rcc.cfgr.hclk(8.mhz()).freeze(&mut flash.acr, &mut pwr);

    let clocks = rcc
        .cfgr
        .msi(hal::rcc::MsiFreq::RANGE100K)
        .freeze(&mut flash.acr, &mut pwr);

    let mut gpioa = dp.GPIOA.split(&mut rcc.ahb2);

    let mut led = gpioa
        .pa3
        .into_push_pull_output(&mut gpioa.moder, &mut gpioa.otyper);

    let mut timer = Delay::new(cp.SYST, clocks);

    loop {
        timer.delay_ms(200_u32);
        led.set_high().ok();
        timer.delay_ms(200_u32);
        led.set_low().ok();
    }
}
