use stm32l4::stm32l4x2::RCC;

#[derive(PartialEq, Eq, Debug)]
pub enum MsiFrequency {
    _100KHZ,
    _200KHZ,
    _400KHZ,
    _800KHZ,
    _1MHZ,
    _2MHZ,
    _4MHZ,
    _8MHZ,
    _16MHZ,
    _24MHZ,
    _32MHZ,
    _48MHZ,
}

impl MsiFrequency {
    pub fn bits(self) -> u8 {
        match self {
            Self::_100KHZ => 0b0000,
            Self::_200KHZ => 0b0001,
            Self::_400KHZ => 0b0010,
            Self::_800KHZ => 0b0011,
            Self::_1MHZ => 0b0100,
            Self::_2MHZ => 0b0101,
            Self::_4MHZ => 0b0110,
            Self::_8MHZ => 0b0111,
            Self::_16MHZ => 0b1000,
            Self::_24MHZ => 0b1001,
            Self::_32MHZ => 0b1010,
            Self::_48MHZ => 0b1011,
        }
    }
}

pub struct MsiClock<'a> {
    pub rcc: &'a RCC,
}

impl<'a> MsiClock<'a> {
    pub fn init(&self, freq: MsiFrequency) -> &Self {
        self.rcc.cr.write(|w| unsafe {
            w.msirgsel()
                .set_bit()
                .msirange()
                .bits(freq.bits())
                .msion()
                .set_bit()
        });

        // Wait for MSI clock to be ready
        while self.rcc.cr.read().msirdy().bit_is_clear() {}

        // Set MSI clock as system clock
        self.rcc.cfgr.modify(|_, w| unsafe { w.sw().bits(0b00) });

        // Check which clock is active, should be MSI
        let sws = self.rcc.cfgr.read().sws().bits();

        if sws != 0b00 {
            panic!("System clock is {} (should be MSI)", sws);
        }

        self
    }
}
