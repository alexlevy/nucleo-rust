macro_rules! define_ptr_type {
    ($name:ident, $ptr:expr) => {
        impl $name {
            fn ptr() -> *const Self {
                $ptr as *const _
            }

            pub fn get() -> &'static Self {
                unsafe { &*Self::ptr() }
            }
        }
    };
}

#[derive(Debug)]
#[repr(C)]
pub struct TsCal1(u16);
define_ptr_type!(TsCal1, 0x1FFF_75A8);
impl TsCal1 {
    pub fn read(&self) -> u16 {
        self.0
    }
}

#[derive(Debug)]
#[repr(C)]
pub struct TsCal2(u16);
define_ptr_type!(TsCal2, 0x1FFF_75AC);
impl TsCal2 {
    pub fn read(&self) -> u16 {
        self.0
    }
}

#[derive(Debug)]
#[repr(C)]
pub struct VrefInt(u16);
define_ptr_type!(VrefInt, 0x1FFF_75AA);
impl VrefInt {
    pub fn read(&self) -> u16 {
        self.0
    }
}
