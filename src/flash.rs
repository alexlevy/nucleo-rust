use stm32l4::stm32l4x2::FLASH;

pub enum Latency {
    ZeroWaitState,
    OneWaitState,
    TwoWaitState,
    ThreeWaitState,
    FourWaitState,
}

impl Latency {
    pub fn bits(self) -> u8 {
        match self {
            Self::ZeroWaitState => 0b000,
            Self::OneWaitState => 0b001,
            Self::TwoWaitState => 0b010,
            Self::ThreeWaitState => 0b011,
            Self::FourWaitState => 0b100,
        }
    }
}

pub struct Flash<'a> {
    pub flash: &'a FLASH,
}

impl<'a> Flash<'a> {
    pub fn init(&self, latency: Latency) -> &Self {
        self.flash
            .acr
            .write(|w| unsafe { w.latency().bits(latency.bits()) });
        self
    }

    pub fn enable_prefetch(&self) -> &Self {
        self.flash.acr.write(|w| w.prften().set_bit());
        self
    }

    pub fn enable_instruction_cache(&self) -> &Self {
        self.flash.acr.write(|w| w.icen().set_bit());
        self
    }

    pub fn enable_data_cache(&self) -> &Self {
        self.flash.acr.write(|w| w.dcen().set_bit());
        self
    }
}
