pub enum OutputSpeed {
    Low = 0b00,
    Medium = 0b01,
    High = 0b10,
    VeryHigh = 0b11,
}

macro_rules! gpio {
    ($($GPIOx:ident, [ $($PXi:expr);* ]),*) => {
        pub mod GPIO {
            use paste::paste;
            use super::OutputSpeed;

            paste! {
                pub fn enable(rcc: &stm32l4::stm32l4x2::RCC) {
                    $(
                        rcc.ahb2enr.modify(|_, w| w.[<gpio $GPIOx:lower en>]().set_bit());
                    )*
                }

                $(
                    $(
                        pub struct [<P $GPIOx $PXi>]<'a> {
                            pub gpio: &'a stm32l4::stm32l4x2::[<GPIO $GPIOx>],
                        }

                        impl [<P $GPIOx $PXi>]<'_> {
                            pub fn as_output(&self) -> &Self {
                                self.gpio.moder.modify(|_, w| w.[<moder $PXi>]().output());
                                self
                            }

                            pub fn push_pull(&self) -> &Self {
                                self.gpio.otyper.modify(|_, w| w.[<ot $PXi>]().clear_bit());
                                self
                            }

                            pub fn open_drain(&self) -> &Self {
                                self.gpio.otyper.modify(|_, w| w.[<ot $PXi>]().set_bit());
                                self
                            }

                            pub fn speed(&self, speed: OutputSpeed) -> &Self {
                                self.gpio.ospeedr.modify(|_, w| w.[<ospeedr $PXi>]().bits(speed as u8));
                                self
                            }

                            pub fn as_input(&self) -> &Self {
                                self.gpio.moder.modify(|_, w| w.[<moder $PXi>]().input());
                                self
                            }

                            pub fn pull_down(&self) -> &Self {
                                self.gpio.pupdr.modify(|_, w| w.[<pupdr $PXi>]().pull_down());
                                self
                            }

                            pub fn pull_up(&self) -> &Self {
                                self.gpio.pupdr.modify(|_, w| w.[<pupdr $PXi>]().pull_up());
                                self
                            }

                            pub fn set_high(&self) -> &Self {
                                self.gpio.odr.modify(|_, w| w.[<odr $PXi>]().set_bit());
                                self
                            }

                            pub fn set_low(&self) -> &Self {
                                self.gpio.odr.modify(|_, w| w.[<odr $PXi>]().clear_bit());
                                self
                            }

                            pub fn read(&self) -> &Self {
                                self.gpio.idr.read().[<idr $PXi>]().bits();
                                self
                            }
                        }
                    )*
                )*
            }
        }
    };
}
