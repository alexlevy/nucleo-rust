use stm32l4::stm32l4x2::Interrupt;

pub fn unmask_interrupt(interrupt: Interrupt) {
    unsafe { stm32l4::stm32l4x2::NVIC::unmask(interrupt) };
}

macro_rules! interrupts {
    ($([ $($line:expr),*; $EXTICRx:ident; $IMRx:ident; $RTSRx:ident; $FTSRx:ident ]),*) => {
        use paste::paste;

        paste! {
            $(
                $(
                    pub struct [<InterruptLine $line>]<'a> {
                        exti: &'a stm32l4::stm32l4x2::EXTI,
                        syscfg: &'a stm32l4::stm32l4x2::SYSCFG,
                    }

                    impl<'a> [<InterruptLine $line>]<'a> {
                        pub fn enable(&self) -> &Self {
                            self.syscfg.$EXTICRx.modify(|_, w| unsafe { w.[<exti $line>]().bits(0b000) });
                            return self;
                        }

                        pub fn unmask(&self) -> &Self {
                            self.exti.$IMRx.modify(|_, w| w.[<mr $line>]().set_bit());
                            return self;
                        }

                        pub fn on_rising_edge(&self, active: bool) -> &Self {
                            if active {
                                self.exti.$RTSRx.modify(|_, w| w.[<tr $line>]().set_bit());
                            } else {
                                self.exti.$RTSRx.modify(|_, w| w.[<tr $line>]().clear_bit());
                            }
                            return self;
                        }

                        pub fn on_falling_edge(&self, active: bool) -> &Self {
                            if active {
                                self.exti.$FTSRx.modify(|_, w| w.[<tr $line>]().set_bit());
                            } else {
                                self.exti.$FTSRx.modify(|_, w| w.[<tr $line>]().clear_bit());
                            }
                            return self;
                        }
                    }
                )*
            )*
        }
    };
}
