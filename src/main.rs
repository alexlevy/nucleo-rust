#![allow(non_snake_case)]
#![allow(unused_variables)]
#![allow(dead_code)]
#![no_std]
#![no_main]

#[macro_use]
extern crate lazy_static;

use core::cell::RefCell;

use cortex_m::interrupt::Mutex;
use cortex_m_rt::{entry, exception};
use defmt_rtt as _;
use panic_probe as _;
use stm32l4::stm32l4x2;
use stm32l4::stm32l4x2::interrupt;

mod clock;
mod flash;
#[macro_use]
mod gpio;
mod power;
mod syscfg;
#[macro_use]
mod interrupts;
#[macro_use]
mod constants;
mod adc;
mod systick;
mod tempsensor;
mod timers;

use adc::ADCResolution;
use clock::{MsiClock, MsiFrequency};
use flash::{Flash, Latency};
use gpio::OutputSpeed;
use interrupts::unmask_interrupt;
use power::{Power, VoltageScaling};
use syscfg::SysCfg;
use systick::Systick;
use tempsensor::{initialize_temperature_sensor, junction_temperature};

interrupts!([8;exticr3;imr1;rtsr1;ftsr1]);
gpio!(A, [3;4;5;8], B, [1]);

lazy_static! {
    static ref MUTEX_GPIOA: Mutex<RefCell<Option<stm32l4x2::GPIOA>>> =
        Mutex::new(RefCell::new(None));
    static ref MUTEX_GPIOB: Mutex<RefCell<Option<stm32l4x2::GPIOB>>> =
        Mutex::new(RefCell::new(None));
    static ref MUTEX_EXTI: Mutex<RefCell<Option<stm32l4x2::EXTI>>> = Mutex::new(RefCell::new(None));
    static ref MUTEX_ADC1: Mutex<RefCell<Option<stm32l4x2::ADC1>>> = Mutex::new(RefCell::new(None));
}

#[entry]
fn main() -> ! {
    let cp = cortex_m::Peripherals::take().unwrap();
    let dp = stm32l4x2::Peripherals::take().unwrap();

    Flash { flash: &dp.FLASH }.init(Latency::ZeroWaitState);

    Power { power: &dp.PWR }.scale(VoltageScaling::Range1);

    MsiClock { rcc: &dp.RCC }.init(MsiFrequency::_8MHZ);

    SysCfg { rcc: &dp.RCC }.enable();

    setup_gpio(&dp.RCC, &dp.GPIOA);

    setup_interrupt_lines(&dp.EXTI, &dp.SYSCFG);

    initialize_temperature_sensor(&dp.RCC, &dp.ADC_COMMON, &dp.ADC1, &dp.TIM6);

    let mut syst = cp.SYST;
    let mut systick = Systick { syst: &mut syst };
    systick.start(2_000_000);

    // Transfer GPIOA and EXTI to a critical section for access in interrupt free zone
    cortex_m::interrupt::free(|cs| {
        MUTEX_GPIOA.borrow(cs).replace(Some(dp.GPIOA));
        MUTEX_GPIOB.borrow(cs).replace(Some(dp.GPIOB));
        MUTEX_EXTI.borrow(cs).replace(Some(dp.EXTI));
        MUTEX_ADC1.borrow(cs).replace(Some(dp.ADC1));
    });

    loop {}
}

fn setup_interrupt_lines(exti: &stm32l4::stm32l4x2::EXTI, syscfg: &stm32l4::stm32l4x2::SYSCFG) {
    let irq8 = InterruptLine8 { exti, syscfg };

    irq8.enable()
        .on_rising_edge(true)
        .on_falling_edge(false)
        .unmask();

    unmask_interrupt(stm32l4::stm32l4x2::Interrupt::EXTI9_5);
}

fn setup_gpio(rcc: &stm32l4::stm32l4x2::RCC, gpioa: &stm32l4::stm32l4x2::GPIOA) {
    GPIO::enable(rcc);

    let pa3 = GPIO::PA3 { gpio: gpioa };
    let pa4 = GPIO::PA4 { gpio: gpioa };
    let pa5 = GPIO::PA5 { gpio: gpioa };

    pa3.as_output().speed(OutputSpeed::High).push_pull();
    pa4.as_output().speed(OutputSpeed::High).push_pull();
    pa5.as_output().speed(OutputSpeed::High).push_pull();

    let pa8 = GPIO::PA8 { gpio: gpioa };

    pa8.as_input().pull_down();
}

#[defmt::panic_handler]
fn panic() -> ! {
    cortex_m::asm::udf()
}

#[exception]
fn SysTick() {
    let temperature = cortex_m::interrupt::free(|cs| {
        let mutex = MUTEX_ADC1.borrow(cs).borrow();
        let adc1 = mutex.as_ref().unwrap();

        adc1.isr.modify(|_, w| w.eoc().set_bit().eos().set_bit());

        adc1.cr.modify(|_, w| w.adstart().set_bit());

        while adc1.isr.read().eoc().bit_is_clear() {}
        let vref_data = adc1.dr.read().regular_data().bits();

        while adc1.isr.read().eoc().bit_is_clear() {}
        let temp_data = adc1.dr.read().regular_data().bits();

        junction_temperature(
            vref_data,
            temp_data,
            ADCResolution::from(adc1.cfgr.read().res().bits()),
        )
    });

    cortex_m::interrupt::free(|cs| {
        let mutex = MUTEX_GPIOA.borrow(cs).borrow();
        let gpioa = mutex.as_ref().unwrap();

        if temperature > 29.567 {
            gpioa
                .odr
                .modify(|_, w| w.odr3().clear_bit().odr4().set_bit().odr5().clear_bit());
        } else if temperature > 29.565 {
            gpioa
                .odr
                .modify(|_, w| w.odr3().clear_bit().odr4().clear_bit().odr5().set_bit());
        } else {
            gpioa
                .odr
                .modify(|_, w| w.odr3().set_bit().odr4().clear_bit().odr5().clear_bit());
        }
    });
}

#[interrupt]
fn EXTI9_5() {
    cortex_m::interrupt::free(|cs| {
        let mutex = MUTEX_EXTI.borrow(cs).borrow();
        let exti = mutex.as_ref().unwrap();

        if exti.pr1.read().pr8().bit_is_set() {
            // Clear the pending bit for EXTI8
            exti.pr1.modify(|_, w| w.pr8().set_bit());
        }
    });
}
