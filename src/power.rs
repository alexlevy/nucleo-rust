use stm32l4::stm32l4x2::PWR;

pub enum VoltageScaling {
    Range1,
    Range2,
}

impl VoltageScaling {
    pub fn bits(self) -> u8 {
        match self {
            Self::Range1 => 0b01,
            Self::Range2 => 0b10,
        }
    }
}

pub struct Power<'a> {
    pub power: &'a PWR,
}

impl<'a> Power<'a> {
    pub fn low_power_mode(&self) -> &Self {
        self.power.cr1.write(|w| w.lpr().set_bit());
        self
    }

    pub fn scale(&self, vos: VoltageScaling) -> &Self {
        self.power
            .cr1
            .write(|w| unsafe { w.vos().bits(vos.bits()) });
        // Wait for power voltage scaling confirmation
        while self.power.sr2.read().vosf().bit_is_set() {}
        self
    }
}
