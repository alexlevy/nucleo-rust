use stm32l4::stm32l4x2::RCC;

pub struct SysCfg<'a> {
    pub rcc: &'a RCC,
}

impl<'a> SysCfg<'a> {
    pub fn enable(&self) {
        self.rcc.apb2enr.modify(|_, w| w.syscfgen().set_bit());
    }
}
