use cortex_m::peripheral::SYST;

pub struct Systick<'a> {
    pub syst: &'a mut SYST,
}

impl<'a> Systick<'a> {
    pub fn start(&mut self, ticks: u32) {
        self.syst.set_reload(ticks);
        self.syst.clear_current();
        self.syst
            .set_clock_source(cortex_m::peripheral::syst::SystClkSource::Core);
        self.syst.enable_interrupt();
        self.syst.enable_counter();
    }

    pub fn delay(&mut self, ticks: u32) {
        self.syst.set_reload(ticks);
        self.syst.clear_current();
        self.syst
            .set_clock_source(cortex_m::peripheral::syst::SystClkSource::Core);
        self.syst.enable_counter();
        while !self.syst.has_wrapped() {}
        self.syst.disable_counter();
    }
}
