use crate::adc::*;
use crate::constants::{TsCal1, TsCal2, VrefInt};
use crate::timers::Tim6;
use stm32l4::stm32l4x2;

pub fn initialize_temperature_sensor(
    rcc: &stm32l4x2::RCC,
    adc_common: &stm32l4x2::ADC_COMMON,
    adc1: &stm32l4x2::ADC1,
    tim6: &stm32l4x2::TIM6,
) {
    let adc = ADC { rcc, adc_common };

    adc.initialize().derive_clock_from_ahb(ADCPrescaler::Hclk4);

    let tim6 = Tim6 { rcc, tim6 };

    tim6.enable().one_pulse_mode().prescaler(0);

    ADC1 { adc1 }
        .power_up(|| {
            tim6.set_counter(160).start();
            while tim6.read() > 0 {}
        })
        .calibrate(|| {
            tim6.set_counter(16).start();
            while tim6.read() > 0 {}
        })
        .enable();

    // Enable vref and temp sensor
    adc_common
        .ccr
        .modify(|_, w| w.vrefen().set_bit().ch17sel().set_bit());

    // Channel selection : Channel 0 (vref) and channel 17 (temp sensor)
    adc1.sqr1
        .modify(|_, w| unsafe { w.l3().bits(2).sq1().bits(0).sq2().bits(17) });

    // Sampling times
    adc1.smpr1
        .modify(|_, w| unsafe { w.smp0().bits(SamplingTime::_12_5Cycles.bits()) });
    adc1.smpr2
        .modify(|_, w| unsafe { w.smp17().bits(SamplingTime::_12_5Cycles.bits()) });

    // Single conversion mode, on software trigger and resolution to 12 bits
    adc1.cfgr.modify(|_, w| unsafe {
        w.cont()
            .clear_bit()
            .exten()
            .bits(ExternalTrigger::Disabled.bits())
            .res()
            .bits(ADCResolution::_12Bits.bits())
    });
}

pub fn junction_temperature(vref_data: u16, sample: u16, res: ADCResolution) -> f32 {
    let tscal_factor = (130.0 - 30.0) / (TsCal2::get().read() as f32 - TsCal1::get().read() as f32);

    let calibration_factor = (3_000.0 * VrefInt::get().read() as f32) / vref_data as f32;

    let calibrated_sample =
        calibration_factor * sample as f32 / ADCResolution::full_scale(res) as f32;

    tscal_factor * (calibrated_sample - TsCal1::get().read() as f32) + 30.0
}
