use paste::paste;
use stm32l4::stm32l4x2;

macro_rules! basic_timers {
    ($($TIMx:expr;$Int:ident),*) => {
        paste! {
            $(
                use stm32l4::stm32l4x2::Interrupt::$Int;

                pub struct [<Tim $TIMx>]<'a> {
                    pub rcc: &'a stm32l4x2::RCC,
                    pub [<tim $TIMx>]: &'a stm32l4x2::[<TIM $TIMx>],
                }

                impl<'a> [<Tim $TIMx>]<'a> {
                    pub fn enable(&self) -> &Self {
                        self.rcc.apb1enr1.modify(|_, w| w.[<tim $TIMx en>]().set_bit());
                        self.rcc.apb1rstr1.modify(|_, w| w.[<tim $TIMx rst>]().set_bit());
                        self.rcc.apb1rstr1.modify(|_, w| w.[<tim $TIMx rst>]().clear_bit());
                        self
                    }

                    pub fn disable(&self) -> &Self {
                        self.[<tim $TIMx>].cr1.modify(|_, w| w.udis().set_bit());
                        self
                    }

                    pub fn one_pulse_mode(&self) -> &Self {
                        self.[<tim $TIMx>].cr1.modify(|_, w| w.opm().set_bit());
                        self
                    }

                    pub fn continuous_mode(&self) -> &Self {
                        self.[<tim $TIMx>].cr1.modify(|_, w| w.opm().clear_bit());
                        self
                    }

                    pub fn clear_interrupt_flag(&self) -> &Self {
                        self.[<tim $TIMx>].sr.write(|w| w.uif().clear_bit());
                        self
                    }

                    pub fn prescaler(&self, value: u16) -> &Self {
                        self.[<tim $TIMx>].psc.write(|w| w.psc().bits(value));
                        // Trigger an update event to load the prescaler value
                        self.[<tim $TIMx>].egr.write(|w| w.ug().set_bit());
                        self.clear_interrupt_flag();
                        self
                    }

                    pub fn enable_interrupt(&self) -> &Self {
                        self.[<tim $TIMx>].dier.modify(|_, w| w.uie().set_bit());
                        unsafe { stm32l4::stm32l4x2::NVIC::unmask($Int) };
                        self
                    }

                    pub fn disable_interrupt(&self) -> &Self {
                        self.[<tim $TIMx>].dier.modify(|_, w| w.uie().clear_bit());
                        stm32l4::stm32l4x2::NVIC::mask($Int);
                        self
                    }

                    pub fn set_counter(&self, value: u16) -> &Self {
                        self.[<tim $TIMx>].cnt.modify(|_, w| w.cnt().bits(value));
                        self
                    }

                    pub fn set_auto_reload(&self, value: u16) -> &Self {
                        self.[<tim $TIMx>].arr.write(|w| w.arr().bits(value));
                        self
                    }

                    pub fn start(&self) {
                        self.[<tim $TIMx>].cr1.modify(|_, w| w.udis().clear_bit().cen().set_bit());
                    }

                    pub fn stop(&self) {
                        self.[<tim $TIMx>].cr1.modify(|_, w| w.udis().set_bit().cen().clear_bit());
                    }

                    pub fn read(&self) -> u16 {
                        self.[<tim $TIMx>].cnt.read().cnt().bits()
                    }
                }
            )*
        }
    };
}

basic_timers!(6;TIM6_DACUNDER, 7;TIM7);
